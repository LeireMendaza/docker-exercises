Las intrucciones COPY y ADD tienen el mismo funcionamiento. Consiste en copiar archivos a un contenedor.

Pero tienen pequeñas diferencias:
•	ADD permite que el parametro sea una URL, COPY no lo permite
•	Si el parámetro de ADD es un archivo comprimido, se descomprimirá. COPY copiara el comprimido.
