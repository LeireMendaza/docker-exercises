ENTRYPOINT indica el ejecutable que usará el contenedor y CMD indica los parametros que se utilizaran con el ejecutable.
Si no se indica el ENTRYPOINT, Docker usara el ejecutable por defecto.

CMD permite definir un comando por defecto, que se ejecuta cuando ejecutas un contenedor sin especificar comando.
Sin embargo, si especificas un comando al ejecutar el contenedor, el comando indicado en el CMD se igmnora.

ENTRYPOINT tambien puedes indicar comandos y parametros. Con la diferencias de que no se ignoran cuando indicas un comando al ejecutar el contenedor.
