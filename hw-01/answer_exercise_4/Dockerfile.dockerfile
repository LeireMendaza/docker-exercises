FROM nginx:1.19.3

COPY ./html/index.html /usr/share/nginx/html/index.html

EXPOSE 8080

HEALTHCHECK --interval=45s --timeout=5s --start-period=1s --retries=2 CMD curl -f http://localhost:8080/
