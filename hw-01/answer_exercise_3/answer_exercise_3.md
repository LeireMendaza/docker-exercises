Pasos:

Crear un dockerfile con las instrucciones indicadas en el enunciado
a.	La imagen nginx versión 1.19.3
b.	Instrucción para copiar el contenido de html en nginx
c.	Exponer puerto 80
Crear carpeta html con un documento .html para mostrar el texto indicado
Ejecutar comandos:
docker build -t ejercicio_3 .
docker run -it --rm -d -p 8080:80 --name ejercicio_3 ejercicio_3
Volúmenes:
Crear el volumen static_content
Comando:
docker run -it --rm -d -p 8080:80 -v static_content:/usr/share/nginx/html --name entrega ejercicio_3
